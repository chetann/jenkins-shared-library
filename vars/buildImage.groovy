#!/usr/bin/env groovy

def call(){
      echo "building the docker image..."
       withCredentials([usernamePassword(credentialsId: 'docker-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t chetanpatil06/java-maven:1.3 ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push chetanpatil06/java-maven:1.3"
}}